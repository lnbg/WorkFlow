﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikeSoft.WorkFlowEngine.Enum;

namespace WikeSoft.WorkFlowEngine.Filter
{
    public class WorkFlowInstanceFilter:BaseWorkFlowFilter
    {
        public string CategoryId { get; set; }

        public FlowRunStatus? FlowRunStatus { get; set; }


        public string AssociatedUserId { get; set; }
    }
}
