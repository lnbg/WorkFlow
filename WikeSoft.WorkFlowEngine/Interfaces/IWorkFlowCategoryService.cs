﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using WikeSoft.WorkFlowEngine.Filter;
using WikeSoft.WorkFlowEngine.Models;

namespace WikeSoft.WorkFlowEngine.Interfaces
{
    public interface IWorkFlowCategoryService
    {
        /// <summary>
        ///  添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        FlowMessage Add(WorkFlowCategoryModel model);

        /// <summary>
        ///  编辑
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        FlowMessage Edit(WorkFlowCategoryModel model);

        /// <summary>
        ///  删除
        /// </summary>
        /// <param name="id">页面ID</param>
        /// <returns></returns>
        FlowMessage Delete(string id);

     

        /// <summary>
        /// 根据父节点的ID，获取子节点
        /// </summary>
        /// <param name="parentId">父节点ID</param>
        /// <returns></returns>
        IList<ZTreeEntity> GetByParentId(string parentId);

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        WorkFlowCategoryModel Find(string id);


        PagedResult<WorkFlowCategoryModel> Query(WorkFlowCategoryFilter filter);
    }
}
