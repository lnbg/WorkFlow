﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class Task
    {
        public String Id { get; set; }

        public String TaskName { get; set; }
    }
}
