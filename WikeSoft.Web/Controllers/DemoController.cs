﻿using System.Collections.Generic;
using System.Web.Mvc;
using Newtonsoft.Json;

using WikeSoft.Enterprise.Interfaces.Sys;

namespace WikeSoft.Web.Controllers
{
    public class DemoController : Controller
    {

        private readonly IPageService _pageService;

        public DemoController(IPageService pageService)
        {
            _pageService = pageService;
        }

        // GET: Demo
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Tabs()
        {
            return View();
        }


        public ActionResult Upload()
        {

            return View();
        }


        
        public ActionResult TreeGrid()
        {

            return View();
        }

        public JsonResult GetData()
        {

            _pageService.GetPagesByUserId(User.Identity.GetLoginUserId());
            var result = new List<object>();
            result.Add(new { Id = 1, Name = "百度科技", Desc = "搜索巨头" });
            result.Add(new { Id = 2, Name = "百度事业部", Desc = "搜索巨头", ParentId = 1 });
            result.Add(new { Id = 3, Name = "百度人事部", Desc = "搜索巨头", ParentId = 1 });
            result.Add(new { Id = 11, Name = "百度HH部", Desc = "搜索巨头", ParentId = 2 });
            result.Add(new { Id = 4, Name = "百度行政", Desc = "搜索巨头", ParentId = 1 });
            result.Add(new { Id = 5, Name = "百度YY部", Desc = "搜索巨头", ParentId = 1 });
            result.Add(new { Id = 12, Name = "百度BB部", Desc = "搜索巨头", ParentId = 2 });
            result.Add(new { Id = 6, Name = "搜狐科技", Desc = "IT" });
            result.Add(new { Id = 7, Name = "搜狐信息部", Desc = "IT", ParentId = 6 });
            result.Add(new { Id = 8, Name = "搜狐人事", Desc = "IT", ParentId = 6 });
            result.Add(new { Id = 9, Name = "搜狐事业部", Desc = "IT", ParentId = 6 });
            result.Add(new { Id = 10, Name = "搜狐事业子部", Desc = "IT", ParentId = 9 });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取导航树
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTrees()
        {
            var trees = _pageService.GetNavTreeModels();
            var treesJson = JsonConvert.SerializeObject(trees);
            return Json(treesJson, JsonRequestBehavior.AllowGet);
        }


    }
}