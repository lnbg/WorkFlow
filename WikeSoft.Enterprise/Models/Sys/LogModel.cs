﻿namespace WikeSoft.Enterprise.Models.Sys
{
    public class LogModel
    {

        

        ///<summary>
        /// 操作内容
        ///</summary>
        public string OperationText { get; set; } // OperationText (length: 200)

        ///<summary>
        /// 用户ID
        ///</summary>
        public string UserId { get; set; } // UserId

        ///<summary>
        /// 用户名
        ///</summary>
        public string UserName { get; set; } // UserName (length: 500)

        ///<summary>
        /// 姓名
        ///</summary>
        public string TrueName { get; set; } // TrueName (length: 200)

        ///<summary>
        /// Ip地址
        ///</summary>
        public string IpAddress { get; set; } // IPAddress (length: 50)

        ///<summary>
        /// URL
        ///</summary>
        public string Url { get; set; } // Url (length: 500)

        ///<summary>
        /// 创建时间
        ///</summary>
        public System.DateTime? CreateDate { get; set; } // CreateDate

         
    }
}
