﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 原料审核状态  0：未审核，1：审核通过，2：审核退回
    /// </summary>
    public enum MaterialStatusEnum
    {
        /// <summary>
        /// 未审核
        /// </summary>
        [Description("未审核")]
        NoAudit = 0,

        /// <summary>
        ///  库存
        /// </summary>
        [Description("库存")]
        Store = 1,

        /// <summary>
        /// 人才部借出
        /// </summary>
        [Description("人才部借出")]
        OutByCustomer = 2,
        
        /// <summary>
        /// 解除合作
        /// </summary>
        [Description("解除合作")]
        RelieveCooperation = 3,

        /// <summary>
        /// 人才退证
        /// </summary>
        [Description("人才退证")]
        HaveBackToCustomer =4,
        
        /// <summary>
        /// 企业部出证
        /// </summary>
        [Description("企业部出证")]
        OutByCompany = 5,
    }
}
