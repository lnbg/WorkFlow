﻿using System;
using System.Collections.Generic;
using WikeSoft.Core;
using WikeSoft.Data.Models;
using WikeSoft.Enterprise.Models.Filters.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Enterprise.Interfaces.Sys
{
    public interface IUserService
    {
        /// <summary>
        /// 添加用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool Add(UserAddModel user);

        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        bool Edit(UserEditModel user);

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool ChangePassword(ChangePasswordModel model);

        /// <summary>
        /// 获取用户
        /// </summary>
        /// <param name="id">用户id</param>
        /// <returns></returns>
        UserEditModel Find(string id);

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ids">用户id集合</param>
        /// <returns></returns>
        bool Delete(IList<string> ids);

        /// <summary>
        /// 验证用户名是否重复
        /// </summary>
        /// <param name="userId">用户id，可以为空，为空表示添加</param>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        bool IsExists(string userId, string userName);
        /// <summary>
        /// 验证员工编号是否重复
        /// </summary>
        /// <param name="userId">用户id，可以为空，为空表示添加</param>
        /// <param name="userCode">员工编号</param>
        /// <returns></returns>
        bool IsExsitUserCode(string userId, string userCode);

        /// <summary>
        /// 验证用户密码是否和上一次密码一样
        /// </summary>
        /// <param name="userId">用户id</param>
        /// <param name="password">新密码</param>
        /// <returns></returns>
        bool IsOldPassword(string userId, string password);

       

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        LoginModel Login(LoginModel model);

        /// <summary>
        /// 分页查询用户信息
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        PagedResult<UserModel> GetUsers(UserFilter filter);

        /// <summary>
        /// 检查用户密码是否过期
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <returns></returns>
        bool IsPasswordExpirated(string userId);
        /// <summary>
        /// 得到上级
        /// </summary>
        /// <param name="getLoginUserId"></param>
        /// <returns></returns>

        UserModel GetLeadShip(string getLoginUserId);

        /// <summary>
        /// 是否为主管
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        bool IsLeader(string userId);


        List<UserModel> GetSubUsers(string userId);
        List<UserModel> GetUsersByDepartment(List<string> departments);
    }
}
