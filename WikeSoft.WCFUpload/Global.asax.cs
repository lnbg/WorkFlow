﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using Microsoft.ServiceModel.Web;

namespace WikeSoft.WCFUpload
{
    public class Global : System.Web.HttpApplication
    {

        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        private void RegisterRoutes()
        {

            WebServiceHost2Factory f2 = new WebServiceHost2Factory();  //可以用于异常处理

            RouteTable.Routes.Add(new ServiceRoute("file", f2, typeof(FileService)));
        }
    }
}